<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function bio(){
        return view('register');
    }
    public function kirim(Request $request){
        $nama = $request->nama;
        return view('welcome', compact('nama'));
    }
}