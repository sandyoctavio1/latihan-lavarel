<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
<h1>Buat Account Baru!</h1>
            <h2>Sign Up Form</h2>
            <form action="/welcome" method ="post">
            @csrf
            <p>First Name:</p>
            <input type="text" name='nama'>
            <p>Last Name:</p>
            <input type="text"> 
            <p>Gender:</p>
            <input type="radio" name="gender" value="0" checked >Laki-Laki <br>
            <input type="radio" name="gender" value="1">Perempuan      
            <p>Nationality:</p>
            <select>
                <option value="ind">Indonesia</option>
                <option value="aus">Australia</option>
                <option value="usa">Amerika</option>
                <option value="chn">China</option>
                <option value="jpn">Jepang</option>
            </select> 
            <p>Language Spoken:</p>
                <input type="checkbox" name="bahasa" value="ind">Indonesia</input> <br>
                <input type="checkbox" name="bahasa" value="aus">Australia</input> <br>
                <input type="checkbox" name="bahasa" value="usa">Amerika</input> <br>
                <input type="checkbox" name="bahasa" value="chn">China</input> <br>
                <input type="checkbox" name="bahasa" value="jpn">Jepang</input>
            <p>Bio:</p>
            <textarea cols="50" rows="10" placeholder="isi bio kamu disini"></textarea> <br>
            <br>
            <input type="submit" value="Sign Up">
        </form>
</body>
</html>